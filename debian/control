Source: python-cinderclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Corey Bryant <corey.bryant@canonical.com>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-ddt,
 python3-fixtures,
 python3-hacking,
 python3-keystoneauth1 (>= 4.3.1),
 python3-openstackdocstheme,
 python3-oslo.i18n,
 python3-oslo.serialization (>= 4.1.0),
 python3-oslo.utils (>= 4.8.0),
 python3-prettytable,
 python3-requests,
 python3-requests-mock,
 python3-simplejson,
 python3-stestr (>= 3.1.0),
 python3-stevedore (>= 3.3.0),
 python3-subunit,
 python3-tempest (>= 1:26.0.0),
 python3-testtools,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-cinderclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-cinderclient.git
Homepage: https://github.com/openstack/python-cinderclient

Package: python3-cinderclient
Architecture: all
Depends:
 python3-keystoneauth1 (>= 4.3.1),
 python3-oslo.i18n,
 python3-oslo.utils (>= 4.8.0),
 python3-pbr,
 python3-prettytable,
 python3-requests,
 python3-simplejson,
 python3-stevedore (>= 3.3.0),
 ${misc:Depends},
 ${python3:Depends},
Provides:
 ${python:Provides},
Description: Python bindings to the OpenStack Volume API - Python 3.x
 Cinder is a block storage as service system for the Openstack cloud computing
 software suite. It is a direct replacement for nova-volume as a separate
 project. Cinder users LVM partitions of your volume servers in order to
 provide iSCSI permanent block storage devices for your virtual machines
 running on Nova.
 .
 This package contains the a client for the OpenStack Volume API. There's a
 Python API (the "cinderclient" module), and a command-line script ("cinder").
 Each implements 100% of the OpenStack Volume API.
 .
 This package provides the Python 3.x module.
